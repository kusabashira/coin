coin
====================
Insert command output on a row-by-row basis

Usage
====================
	coin [OPTION] COMMAND [ARGS]

	Options:
    	--help     display this help and exit
    	--version  output version information and exit

Example
====================
	$ yes Hello | seq -f%-2g 1 5
	1 Hello
	2 Hello
	3 Hello
	4 Hello
	5 Hello

	$ seq -f%2g 3 7 | coin seq -f%2g 1 5 | coin seq -f%2g 1 5
	 1 2 3
	 2 3 4
	 3 4 5
	 4 5 6
	 5 6 7

License
====================
MIT License

author
====================
Kusabashira <kusabashira227@gmail.com>

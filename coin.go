package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"os/exec"
)

func version() {
	os.Stderr.WriteString(`coin: v0.1.0
`)
}

func usage() {
	os.Stderr.WriteString(`Concatenate command output on row-by-row basis
Usage: coin [OPTION] COMMAND {ARGS}

Options:
	--help       show this help message and exit
	--version    print the version and exit

Report bugs to <kusabashira227@gmail.com>
`)
}

func _main() (exitCode int, err error) {
	var isHelp bool
	var isVersion bool
	flag.BoolVar(&isHelp, "help", false, "show this help message and exit")
	flag.BoolVar(&isVersion, "version", false, "print the version and exit")
	flag.Parse()

	if isHelp {
		usage()
		return 0, nil
	}

	if isVersion {
		version()
		return 0, nil
	}

	if flag.NArg() < 1 {
		usage()
		return 1, nil
	}

	co := exec.Command(flag.Arg(0), flag.Args()[1:]...)
	coout, err := co.StdoutPipe()
	if err != nil {
		return 1, err
	}
	defer coout.Close()

	if err := co.Start(); err != nil {
		return 1, err
	}

	coScan := bufio.NewScanner(coout)
	inScan := bufio.NewScanner(os.Stdin)
	for coScan.Scan() && inScan.Scan() {
		fmt.Print(coScan.Text())
		fmt.Print(inScan.Text())
		fmt.Println()
	}
	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "coin:", err)
	}
	os.Exit(exitCode)
}
